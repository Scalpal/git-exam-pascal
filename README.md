# Déroulement de l'examen

Vous devez tout d'abord créer un fork **privé** de ce dépôt sur votre compte gitlab.

Vous devez ensuite m'ajouter en tant que mainteneur - kr1 - du dépôt afin que j'ai accès à votre travail.

Vous devez répondre aux questions de la première partie dans une branche exam-part-1.
Vous devez répondre aux questions de la seconde partie dans une branche exam-part-2.
Vous devez répondre aux questions de la troisième partie dans une branche exam-part-3.

Une fois que c'est fait, vous pouvez merger les trois branche dans la branche principale !

Si vous ne réussissez pas à gérer les branches comme il le faut, pas de panique. Vous pouvez copier le texte et m'envoyer les réponses dans Teams.

## Questions

Dans les deux premières parties, répondez aux questions en ne laissant que la ou les bonnes réponses.

Dans la troisième partie, insérez la réponse sous la questions. Vous devez utiliser vos mots / vos phrases pour répondre, pas de copier coller.

### Première partie (une seule bonne réponse)

1	Quelle est la commande qui permet de télécharger un dépôt sur votre ordinateur ?		
		C	git clone

2	Quelle commande permet d’envoyer le code sur le dépôt distant ?		
		A	git push

3	Quelle commande permet de vérifier dans quel état est notre dépôt local ?		
		B	git status

4	Dans quelle zone doit-on placer les éléments pour les sauvegarder avant de les partager ?		
		C	Index

5	Comment démarrer un nouveau projet avec git ?	
		C	git init

6	Comment récupérer uniquement les dernières mise à jour d’un dépôt ?		
		A	git pull

7	Avec quelle commande ajoute-t-on ses modifications avant de pouvoir faire un commit ?	
		A	git add

8	Avec quelle commande peut-on vérifier qui a fait la dernière modification sur un fichier ?
		D	git blame

9	Quelle commande permet d’ajouter un message à un commit ?		
		B	git commit -m "mon message"

10	Nous venons de créer un nouveau fichier appelé «fichier.html». Lequel des éléments suivants ajoutera ce fichier afin que nous puissions le commiter dans git?		
		D	git add fichier.html 

11	Comment retourner à la branche principale nommée master ?	
		A	git checkout master 

12	Quelle est la commande pour annuler un commit s’il a déjà été poussé ?		
		B	git revert <commit> 

13	Quelle est la commande pour annuler un commit s’il n’est pas encore dans le dépôt distant ?		
		C	git reset <commit> 

14	Comment créer une nouvelle branche et y basculer directement?		
		C	git checkout -b <branch-name> 

### Deuxième partie (à partir de là plusieurs choix sont parfois possibles)

15	Qu’est-ce qu’une branche dans git ?		
		B	Un espace de travail
			
16	Comment faire pour rassembler plusieurs commits en un seul ?		
		A	git merge commit1 commit2 … commitN
			
17	Comment ajouter un nouveau dépôt distant ?		
		A	git init -a <name> <url> 
			
18	A quoi sert un tag dans git ?		
		A	Marquer un point spécifique du développement pour pouvoir y revenir facilement
		C	Marquer une version de livraison du produit
			
19	A quoi sert un git rebase ?		
		C	Déplacer des commits d’une branche sur une autre
		D	Regrouper des commits
			
20	A quoi sert git show ?		
		A	ça affiche le message d’un commit et son diff
		B	ça affiche le message d’un tag et ses objets référencés
			
21	Le développement du projet est réalisé dans une branche dev. La version distribuée est celle de la branche master. On souhaite intégrer les nouveaux développements dans la version distribuée. Que doit on faire ?		
		C	git checkout master;   git merge dev
			
22	Comment enlever un fichier d'un commit que l'on vient d'effectuer, sans supprimer le fichier du répertoire de travail ? 		
		C	git checkout HEAD^; git reset HEAD fichier; git commit
			
23	Dans le modèle standard git, dans quelle(s) branche(s) doit-on merger une branche « hotfix » ?		
		A	master
			
24	git est le meilleur outil de gestion de version !		
		A	VRAI
		D	Réponse D


### Troisième partie : questions à réponse libre

25	Comment un conflit dans git peut-il être résolu ?		
			
	Dans Git, pour résoudre un conflit, il faut parfois simplement lire l'erreur qui est affichée car la plupart du temps la solution au problème est écrite aussi.
			

26	Pour supprimer une branche, quelle commande est utilisée ?		
			
	Pour supprimer une branche, on utilise la commande : git branch -d <nom de la branche> 
			

27	Quelles sont les étapes pour effectuer des changements dans un dépôt Github qui ne vous appartient pas ?		
			
	1- On fork le repo 
	2- On crée une branche pour effectuer nos changements sur ce même repo 
	3- On add , commit , push nos changements sur notre branche 
	4- On fait une pull request au propriétaire du dépôt Github afin qu'il récupère les changements effectués
			

28	A quoi sert « .gitignore » ?		
			
	.gitignore permet d'avoir un fichier qui contient les noms des fichiers à ignorer, c'est-à-dire par exemple lorsque l'on effectue des modifications sur ces fichiers, ils ne soient pas pris en compte lors d'un git status / git add, etc...	
	Ou éventuellement pour des ignorer des fichiers très lourd qui ne devraient pas être pris en compte aussi.
			

29	Quels sont les avantages d’utiliser git ?		
			
	Les avantages d'utiliser git sont : 

		- de pouvoir travailler sur un projet à plusieurs SANS se "marcher sur les pieds" lorsque chacun fais des modifications, développe une feature ou règle un bug, 
		- de pouvoir gérer les versions du projet plus facilement avec un historique des modifications (historique des commits) qui sont ajoutées au projet 
		- d'avoir une "back-up" grâce à cette gestion de versions, dans le cas où un commit aurait "cassé" quelque chose, il est beaucoup plus simple de trouver le problème et de retourner à une version qui fonctionnait  
			

30	Qu’est-ce qu’un « fork » ?		
			
	Un fork est comme le mot l'indique, on ("pique" comme une fourchette) récupère le repo d'une personne pour pouvoir  travailler dessus, ou s'en servir.
			
